DOCKER_NAME=quay.io/schneevonmorgen/friedrich
DOCKER_TAG=$(shell hg id -b)-$(shell hg id -i)-$(shell hg id -n)

rtfm:
	cat Makefile

clean_pyc:
	find . -name '*.pyc' -delete

force_committed:
	if [ $(shell hg id -n | grep \+) ]; then echo 'Found uncommitted changes!'; exit 1; fi

docker_build: force_committed clean_pyc
	docker build -t $(DOCKER_NAME):$(DOCKER_TAG) .

docker_build_uncommitted: clean_pyc
	docker build -t $(DOCKER_NAME):$(shell echo $(DOCKER_TAG) | sed s/+/dirty/g) .

docker_push: 
	docker push $(DOCKER_NAME):$(DOCKER_TAG)

docker_run:
	docker run --rm -ti $(DOCKER_NAME):$(DOCKER_TAG)

docker_sh:
	docker run --rm -ti $(DOCKER_NAME):$(DOCKER_TAG) sh

docker_all: docker_build docker_push
