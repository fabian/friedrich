FROM alpine:3.6

MAINTAINER Fabian Topfstedt <topfstedt@schneevonmorgen.com>

WORKDIR /app
COPY . /app

RUN rm -rf /app/.hg \ 
 && apk --no-cache --update add \
    python \
    py-pip \
    supervisor \
    ca-certificates \
    tzdata \
 && apk --no-cache add --virtual build-dependencies \
    python-dev \
    build-base \
    mercurial \
    linux-headers \
 && pip install --upgrade \
    superlance \
    virtualenv \
    pip \
 && mkdir /venv \
 && virtualenv /venv/friedrich \
 && /venv/friedrich/bin/pip install --upgrade pip \
 && /venv/friedrich/bin/pip install -r /app/requirements.txt \
 && /venv/friedrich/bin/pip install -r /app/requirements_testing.txt \
 && apk del build-dependencies \
 && mkdir -p /etc/supervisor.d \
 && ln -s /app/supervisor.ini /etc/supervisor.d/friedrich.ini \
 && /venv/friedrich/bin/nosetests /app/tests

ENV PYTHONPATH=/app \
    FRIEDRICH_HTTP_ADDRESS=localhost \
    FRIEDRICH_HTTP_FLUSH_ENDPOINT=/flush/ \
    FRIEDRICH_HTTP_PORT=8124 \
    FRIEDRICH_RIEMANN_PORT=5555 \
    FRIEDRICH_RIEMANN_PROTOCOL=UDP \
    FRIEDRICH_TTL=120 \
    TZ=Europe/Vienna

EXPOSE 8125

CMD ["/usr/bin/supervisord", "-n"]
