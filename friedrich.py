import os

from friedrich.server import serve_forever


def welcome():
    print("Starting Friedrich...")
    friedrich_envs = (
        (k, os.environ[k],) for \
        k in sorted(os.environ.keys()) if \
        k.startswith('FRIEDRICH_')
    )
    for key, value in friedrich_envs:
        print("%s: %s" % (key, value))


if __name__ == '__main__':
    welcome()
    serve_forever()
