import os

from friedrich.translator import StatsDTranslator
import requests

FRIEDRICH_HTTP_ADDRESS = os.environ.get('FRIEDRICH_HTTP_ADDRESS', 'localhost')
FRIEDRICH_HTTP_PORT = os.environ.get('FRIEDRICH_HTTP_PORT', 8124)
FRIEDRICH_HTTP_ENDPOINT = 'http://%s:%s/aggregate/' % (FRIEDRICH_HTTP_ADDRESS, 
                                                       FRIEDRICH_HTTP_PORT)


def callback_fn(stat):
    requests.post(FRIEDRICH_HTTP_ENDPOINT, json=stat._asdict())


def main():
    translator = StatsDTranslator(callback_fn=callback_fn)
    translator.translate_forever()


if __name__ == '__main__':
    main()
