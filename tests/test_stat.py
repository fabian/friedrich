from unittest import TestCase

from friedrich.stat import append_suffix, Stat, statsd_to_stat


class StatTests(TestCase):
    def test_statsd_to_stat_with_timing(self):
        statsd_msg = 'i.am.a.timing,tagkey1=tagval1,tagkey2=tagval2:123|ms'
        stat = statsd_to_stat(statsd_msg)
        self.assertEquals(stat.name, 'i.am.a.timing')
        self.assertEquals(stat.tags, 'tagkey1=tagval1,tagkey2=tagval2')
        self.assertEquals(stat.value, 123)
        self.assertEquals(stat.kind, 'ms')

    def test_statsd_to_stat_with_counter(self):
        statsd_msg = 'i.am.a.counter,tagkey1=tagval1,tagkey2=tagval2:1|c'
        stat = statsd_to_stat(statsd_msg)
        self.assertEquals(stat.name, 'i.am.a.counter')
        self.assertEquals(stat.tags, 'tagkey1=tagval1,tagkey2=tagval2')
        self.assertEquals(stat.value, 1)
        self.assertEquals(stat.kind, 'c')

    def test_statsd_to_stat_unpacks_tags(self):
        statsd_msg = 'i.am.a.counter,tagkey1=tagval1,tagkey2=tagval2:1|c'
        stat = statsd_to_stat(statsd_msg, unpack_tags=True)
        self.assertEquals(stat.tags, dict(tagkey1='tagval1',
                                          tagkey2='tagval2'))

    def test_statsd_to_stat_without_tags(self):
        statsd_msg = 'i.am.a.counter:1|c'
        stat = statsd_to_stat(statsd_msg)
        self.assertEquals(stat.tags, None)

    def test_statsd_to_stat_without_tags_but_unpack_tags_set_to_true(self):
        statsd_msg = 'i.am.a.counter:1|c'
        stat = statsd_to_stat(statsd_msg, unpack_tags=True)
        self.assertEquals(stat.tags, {})

    def test_statsd_to_stat_name_prefix(self):
        statsd_msg = 'i.am.a.counter:1|c'
        stat = statsd_to_stat(statsd_msg, name_prefix='fr/')
        self.assertEquals(stat.name, 'fr/i.am.a.counter')

    def test_append_suffix_1(self):
        stat = 'i.am.a.timing,tag=foo'
        suffix = '/95th'
        stat_with_suffix = append_suffix(stat, suffix)
        self.assertEquals(stat_with_suffix, 'i.am.a.timing/95th,tag=foo')

    def test_append_suffix_2(self):
        stat = 'i.am.a.timing'
        suffix = '/95th'
        stat_with_suffix = append_suffix(stat, suffix)
        self.assertEquals(stat_with_suffix, 'i.am.a.timing/95th')
