import collections
import random
from unittest import TestCase

import mock

from friedrich.aggregator import Aggregator


class AggregatorTests(TestCase):

    def setUp(self):
        host = 'server1.topfstedt.de'
        self.aggregator = Aggregator(host=host)


    def test_setup(self):
        self.assertTrue(self.aggregator.first)
        self.assertTrue(self.aggregator.last)
        self.assertTrue(isinstance(self.aggregator.counter,
                                   collections.Counter))

    def test_reset(self):
        self.aggregator.first = None
        self.aggregator.last = None
        self.aggregator.counter = None
        self.aggregator.timings = None

        self.aggregator.reset()

        self.assertIsNotNone(self.aggregator.first)
        self.assertIsNotNone(self.aggregator.last)
        self.assertIsNotNone(self.aggregator.counter)
        self.assertIsNotNone(self.aggregator.timings)

    def test_flush_calls_reset(self):
        self.aggregator.reset = mock.MagicMock()
        self.aggregator.flush()
        self.assertTrue(self.aggregator.reset.called)

    def test_flush(self):
        self.aggregator.counter_incr('foo')
        self.aggregator.timing_add('bar', 1000)
        events = self.aggregator.flush()
        for event in events:
            self.assertTrue(event['metric'])
            self.assertTrue(event['time'])
            self.assertTrue(event['host'])
            self.assertTrue(event['service'])
            self.assertTrue(event['ttl'])

    def test_counter_read(self):
        stat = 'foo'
        self.assertEqual(self.aggregator.counter_read(stat), 0)

    def test_counter_incr(self):
        stat = 'foo'
        self.aggregator.counter_incr(stat)
        self.assertEqual(self.aggregator.counter_read(stat), 1)

    def test_counter_incr_by_8(self):
        stat = 'foo'
        self.aggregator.counter_incr(stat, 8)
        self.assertEqual(self.aggregator.counter_read(stat), 8)

    @mock.patch('friedrich.aggregator.random')
    def test_counter_incr_with_rate(self, random_mock):
        stat = 'foo'

        # Should get counted
        random_mock.random.return_value = 0.5
        self.aggregator.counter_incr(stat, rate=0.5)
        self.assertEqual(self.aggregator.counter_read(stat), 1)

        # Should not get counted
        random_mock.random.return_value = 0.51
        self.aggregator.counter_incr(stat, rate=0.5)
        self.assertEqual(self.aggregator.counter_read(stat), 1)

    def test_counter_incr_updates_last_attr(self):
        self.aggregator.last = None
        self.aggregator.counter_incr('foo')
        self.assertTrue(self.aggregator.last)

    def test_counter_decr(self):
        stat = 'foo'
        self.aggregator.counter_decr(stat)
        self.assertEqual(self.aggregator.counter_read(stat), -1)

    def test_counter_decr_by_8(self):
        stat = 'foo'
        self.aggregator.counter_decr(stat, 8)
        self.assertEqual(self.aggregator.counter_read(stat), -8)

    def test_counter_decr_updates_last_attr(self):
        self.aggregator.last = None
        self.aggregator.counter_decr('foo')
        self.assertTrue(self.aggregator.last)

    def test_counter_as_events(self):
        self.aggregator.counter_incr('foo')
        for event in self.aggregator._counter_as_events():
            self.assertTrue(event['metric'])
            self.assertTrue(event['time'])
            self.assertTrue(event['host'])
            self.assertTrue(event['service'])
            self.assertTrue(event['ttl'])

    def test_timing_add(self):
        self.aggregator.timing_add('foo', 100, rate=1)
        self.assertEquals(self.aggregator.timings['foo'], [100])

    def test_timing_add_insorts_values(self):
        self.aggregator.timing_add('foo', 5, rate=1)
        self.assertEquals(self.aggregator.timings['foo'], [5])
        self.aggregator.timing_add('foo', 6, rate=1)
        self.assertEquals(self.aggregator.timings['foo'], [5, 6])
        self.aggregator.timing_add('foo', 3, rate=1)
        self.assertEquals(self.aggregator.timings['foo'], [3, 5, 6])

    def test_timing_read(self):
        self.aggregator.timings['foo'] = [100]
        self.assertEquals(self.aggregator.timing_read('foo'), [100])

    @mock.patch('friedrich.aggregator.random')
    def test_timing_add_with_rate(self, random_mock):
        stat = 'foo'

        # Should add timing
        random_mock.random.return_value = 0.5
        self.aggregator.timing_add(stat, 500, rate=0.5)
        self.assertEqual(self.aggregator.timing_read(stat), [500])

        # Should not add timing
        random_mock.random.return_value = 0.51
        self.aggregator.timing_add(stat, 510, rate=0.5)
        self.assertEqual(self.aggregator.timing_read(stat), [500])

    def test_timings_stats(self):
        stat = 'foo'

        zero_to_ninetynine = range(100)
        random.shuffle(zero_to_ninetynine)

        for i in zero_to_ninetynine:
            self.aggregator.timing_add(stat, i)

        timing_stats = self.aggregator.timing_stats(stat)
        self.assertEquals(timing_stats['min'], 0)
        self.assertEquals(timing_stats['max'], 99)
        self.assertEquals(timing_stats['avg'], 49.5)
        self.assertEquals(timing_stats['95th'], 94.05)

    def test_timings_as_events(self):
        self.aggregator.timing_add('foo', 100)
        self.aggregator.timing_add('foo', 200)

        self.aggregator.timing_add('bar', 100)
        self.aggregator.timing_add('bar', 200)

        for event in self.aggregator._timings_as_events():
            self.assertTrue(event['metric'])
            self.assertTrue(event['time'])
            self.assertTrue(event['host'])
            self.assertTrue(event['service'])
            self.assertTrue(event['ttl'])
