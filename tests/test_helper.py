from unittest import TestCase

from friedrich.helper import average, percentile, unix_timestamp


class AverageTests(TestCase):
    def test_average_empty_raises_valueerror(self):
        self.assertRaises(ValueError, average, [])

    def test_average_of_one_element(self):
        self.assertEquals(average([0]), 0)

    def test_average_of_multiple_elements(self):
        self.assertEquals(average([1, 3]), 2)

    def test_average_of_multiple_elements_2(self):
        self.assertEquals(average([0.12345, 1.2345]), 0.678975)

    def test_average_of_generator(self):
        gen = (x for x in [1,3])
        self.assertEquals(average(gen), 2)


class PercentileTests(TestCase):
    def test_percentile_empty_raises_valueerror(self):
        self.assertRaises(ValueError, percentile, [], 95)

    def test_percentile_of_one_element_1(self):
        self.assertEquals(percentile([100], 95), 0)

    def test_percentile_of_one_element_1(self):
        self.assertEquals(percentile([100], 95), 100)

    def test_percentile_of_multiple_elements_1(self):
        self.assertEquals(percentile([90, 110], 50), 100)

    def test_percentile_of_multiple_elements_2(self):
        self.assertEquals(percentile([90, 110], 95), 109)

    def test_percentile_of_multiple_elements_3(self):
        self.assertEquals(percentile([0.1234, 1.234], 95), 1.17847)

    def test_percentile_do_sort(self):
        self.assertEquals(percentile([110, 90], 95, do_sort=True), 109)

    def test_percentile_of_generator(self):
        gen = (x for x in [90, 110])
        self.assertEquals(percentile(gen, 95), 109)


class UnixTimestampTests(TestCase):

    def test_unix_timestamp_1(self):
        unix_ts = unix_timestamp()
        unix_ts + 0
        self.assertTrue(unix_ts)
