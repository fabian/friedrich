import datetime
import math


def average(iterable):
    """
    The average of the iterable, rounded after round_ndigits.
    """
    values = iterable if not hasattr(iterable, 'next') else list(iterable)
    try:
        result = sum(values) / float(len(values))
    except ZeroDivisionError:
        raise ValueError('Cannot calculate average of empty iterable')
    else:
        return result

def percentile(iterable, q, do_sort=False):
    """
    The q-th percentile (e.g. 95) of the given iterable.
    """
    values = iterable if not hasattr(iterable, 'next') else list(iterable)
    if do_sort:
        values = sorted(values)

    values_len = len(values)
    if values_len == 1:
        return values[0]
    elif values_len == 0:
        raise ValueError('Cannot calculate percentile of empty iterable')

    prcntl = q / 100.
    k = int(math.floor(prcntl * (values_len - 1) + 1)) - 1
    f = (prcntl * (values_len - 1) + 1) % 1
    result = values[k] + (f * (values[k + 1] - values[k]))
    return result

def unix_timestamp(dt=None):
    """
    Returns the unix timestamp of a given datetime instance as an
    integer. Defaults to the current UTC timestamp.
    """
    if dt is None:
        dt = datetime.datetime.utcnow()
    return int((dt - datetime.datetime(1970, 1, 1)).total_seconds())
