import bisect
import collections
import itertools
import random

from .helper import average, percentile, unix_timestamp
from .stat import append_suffix


class Aggregator(object):

    def __init__(self, host, ttl=120):
        self.host = host
        self.ttl = ttl

        self.reset()

    def reset(self):
        """
        Resets timestamps and objects where counters and timings are
        stored in.
        """
        unix_ts = unix_timestamp()
        self.first, self.last = unix_ts, unix_ts

        self.counter = collections.Counter()
        self.timings = collections.defaultdict(lambda: [])

    def flush(self):
        """
        Returns a generator of all values (counters and timings) as
        events and resets the Aggregator instance.
        """
        values = itertools.chain(
            self._counter_as_events(),
            self._timings_as_events(),
        )
        self.reset()
        return values

    def counter_read(self, stat):
        """
        Returns the current count of `stat`.
        """
        return self.counter[stat]

    def counter_incr(self, stat, count=1, rate=1):
        """
        Increases the counter `stat` by `count`.
        By settings `rate`, the percentage of counter increases can
        be adjusted. It ranges from 0 (0 %) to 1 (100 %).
        Returns True if the counter got increased, False otherwise.
        """
        if rate < 1 and random.random() > rate:
            return False

        self.last = unix_timestamp()
        self.counter[stat] += count
        return True

    def counter_decr(self, stat, count=1, rate=1):
        """
        Decreases the counter `stat` by `count`.
        By settings `rate`, the percentage of counter decreases can
        be adjusted. It ranges from 0 (0 %) to 1 (100 %).
        Returns True if the counter got decreased, False otherwise.
        """
        return self.counter_incr(stat=stat, count=-1 * count, rate=rate)

    def _counter_as_events(self):
        """
        Returns a generator of all counter sums as events.
        """
        return (
            dict(host=self.host, service=counter, metric=count,
                 ttl=self.ttl, time=self.last)
            for counter, count in self.counter.iteritems()
        )

    def timing_read(self, stat):
        """
        Returns the list of stores time deltas for `stat`.
        """
        return self.timings[stat]

    def _timings_as_events(self, q=95):
        """
        Returns a generator of all timings, aggregated by calculating
        the qth (defaults to 95th) percentile, as events.
        """
        return (
            dict(host=self.host, service=append_suffix(stat, '/%dth' % q),
                 metric=percentile(timings, q), ttl=self.ttl, time=self.last)
            for stat, timings in self.timings.iteritems()
        )

    def timing_stats(self, stat):
        """
        Returns the stored timings of `stat`.
        """
        timings = self.timing_read(stat)

        return {
            'avg': average(timings),
            'min': min(timings),
            'max': max(timings),
            '95th': percentile(timings, 95),
        }

    def timing_add(self, stat, delta, rate=1):
        """
        Stores the delta of the timing `stat`.
        By settings `rate`, the percentage of delta appends can
        be adjusted. It ranges from 0 (0 %) to 1 (100 %).
        Returns True if delta got added, False otherwise.
        """
        if rate < 1 and random.random() > rate:
            return False

        self.last = unix_timestamp()
        bisect.insort(self.timings[stat], delta)
        return True
