import os
import SocketServer

from .stat import statsd_to_stat

FRIEDRICH_AGGREGATOR_HOST = os.environ.get('FRIEDRICH_AGGREGATOR_HOST',
                                           'localhost')
FRIEDRICH_AGGREGATOR_PORT = os.environ.get('FRIEDRICH_AGGREGATOR_PORT', 8125)




class StatsDUDPHandler(SocketServer.BaseRequestHandler):
    """
    StatsD parsing handler for a SocketServer that calls the servers
    callback_fn method every time it parses a stat.
    """

    def handle(self):
        """
        Receives data, calls the parse method and triggers the
        servers callback_fn method for every parsed stat.
        """
        statsd_msg, socket = self.request
        stat = statsd_to_stat(statsd_msg)
        if stat:
            self.server.callback_fn(stat)


class StatsDTranslator(object):
    """
    Blocking translator that listens to UDP StatsD data, translates it
    to a named tuple and triggered the given callback_fn method with it.

    Create an instance and call its translate_forever method.
    """
    def __init__(self, callback_fn, host=FRIEDRICH_AGGREGATOR_HOST,
                 port=FRIEDRICH_AGGREGATOR_PORT):
        self.server = SocketServer.UDPServer((host, port), StatsDUDPHandler)
        self.server.callback_fn = callback_fn

    def translate_forever(self):
        """
        Start blocking translation.
        """
        try:
            self.server.serve_forever()
        except (KeyboardInterrupt, SystemExit):
            raise
