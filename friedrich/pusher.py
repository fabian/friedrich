import os

import bernhard

from .helper import unix_timestamp

FRIEDRICH_RIEMANN_HOSTS = os.getenv('FRIEDRICH_RIEMANN_HOSTS', '').split(',')
FRIEDRICH_RIEMANN_PORT = int(os.getenv('FRIEDRICH_RIEMANN_PORT', 5555))
FRIEDRICH_RIEMANN_PROTOCOL = os.getenv('FRIEDRICH_RIEMANN_PROTOCOL', 'UDP')


class Pusher(object):
    def __init__(self, hosts=FRIEDRICH_RIEMANN_HOSTS,
                 port=FRIEDRICH_RIEMANN_PORT,
                 protocol=FRIEDRICH_RIEMANN_PROTOCOL):
        self.hosts = hosts
        self.port = port
        self.protocol = protocol

    def _get_clients(self):
        try:
            transport = getattr(bernhard,
                                '%sTransport' % self.protocol.upper())
        except AttributeError:
            raise Exception('Protocol %s is not an option' % self.protocol)
        else:
            return [
                bernhard.Client(host=host, port=self.port, transport=transport)
                for host in self.hosts
            ]

    def push(self, values):
        unix_ts = unix_timestamp()

        unexpired_values = [
            value
            for value in values
            if unix_ts <= value['time'] + value['ttl']
        ]

        for client in self._get_clients():
            for unexpired_value in unexpired_values:
                client.send(unexpired_value)
