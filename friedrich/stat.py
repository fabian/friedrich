from collections import namedtuple
import os
import re

FRIEDRICH_NAME_PREFIX = os.getenv('FRIEDRICH_NAME_PREFIX', '')

STATSD_STAT_PATTERN = re.compile(r''
    '(?P<name>[^,:]+)'
    ',?'
    '(?P<tags>[^:]+)?'
    ':'
    '(?P<value>[\d]+)'
    '\|'
    '(?P<kind>[\w]+)'
)


Stat = namedtuple('Stat', ['name', 'tags', 'value', 'kind'])


def statsd_to_stat(statsd_msg, unpack_tags=False, pattern=STATSD_STAT_PATTERN,
                   name_prefix=FRIEDRICH_NAME_PREFIX):
    """
    Parses a statsd message and returns either its details as a
    namedtuple or None (if parsing failed).
    """
    match = pattern.match(statsd_msg)
    if not match:
        return None

    groupdict = match.groupdict()

    name = '%s%s' % (name_prefix, groupdict.get('name'))
    tags_default = {} if unpack_tags else None
    tags = groupdict.get('tags') or tags_default
    if unpack_tags and tags:
        tags = dict([
            item.split('=')
            for item in tags.split(',')
        ])
    value = int(groupdict.get('value'))
    kind = groupdict.get('kind')

    return Stat(name=name, tags=tags, value=value, kind=kind)


def append_suffix(stat_str, suffix, tags_sep=','):
    """
    Appends the suffix to the stat string before any tags.
    """
    splitted = stat_str.split(tags_sep)
    splitted[0] = "%s%s" % (splitted[0], suffix)
    return tags_sep.join(splitted)
