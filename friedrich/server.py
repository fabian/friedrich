import json
import os
import socket

from svm_kubernetes import pod
import tornado.ioloop
import tornado.web

from .aggregator import Aggregator
from .pusher import Pusher
from .stat import Stat


FRIEDRICH_HOST = os.environ.get('FRIEDRICH_HOST', socket.gethostname())
if os.environ.get('FRIEDRICH_KUBERNETES_HOST_IP_AS_HOST'):
    sa_client = pod.ServiceAccountClient()
    host_ip = sa_client.get_host_ip()
    FRIEDRICH_HOST = host_ip.replace('.', '-') if host_ip else FRIEDRICH_HOST
FRIEDRICH_TTL = int(os.environ.get('FRIEDRICH_TTL', 120))
FRIEDRICH_HTTP_ADDRESS = os.environ.get('FRIEDRICH_HTTP_ADDRESS', 'localhost')
FRIEDRICH_HTTP_PORT = int(os.environ.get('FRIEDRICH_HTTP_PORT', 8124))
FRIEDRICH_RIEMANN_HOSTS = os.getenv('FRIEDRICH_RIEMANN_HOSTS', '').split(',')
FRIEDRICH_RIEMANN_PORT = int(os.getenv('FRIEDRICH_RIEMANN_PORT', 5555))
FRIEDRICH_RIEMANN_PROTOCOL = os.getenv('FRIEDRICH_RIEMANN_PROTOCOL', 'UDP')


class BaseHandler(tornado.web.RequestHandler):
    """
    A handler that carries an aggregator instance.
    """
    def initialize(self, aggregator):
        self.aggregator = aggregator


class AggregateHandler(BaseHandler):
    def post(self):
        """
        Expects to receive a JSON pickled Stat object to aggregate.
        """
        data = json.loads(self.request.body)
        stat = Stat(**data)
        name = stat.name if not stat.tags else "%s,%s" % (stat.name, stat.tags)

        if stat.kind == "c":
            self.aggregator.counter_incr(stat=name, count=stat.value)
        elif stat.kind == "ms":
            self.aggregator.timing_add(stat=name, delta=stat.value)


class FlushHandler(BaseHandler):
    def post(self):
        """
        Flushes the aggregator and sends all values to all Riemann
        hosts.
        """
        values = self.aggregator.flush()
        pusher = Pusher(hosts=FRIEDRICH_RIEMANN_HOSTS,
                        port=FRIEDRICH_RIEMANN_PORT,
                        protocol=FRIEDRICH_RIEMANN_PROTOCOL)
        pusher.push(values)

    def get(self):
        """
        See post.
        """
        return self.post()


def make_app(host=FRIEDRICH_HOST, ttl=FRIEDRICH_TTL):
    aggregator = Aggregator(host=host, ttl=ttl)

    return tornado.web.Application([
        (r"/aggregate/", AggregateHandler, dict(aggregator=aggregator)),
        (r"/flush/", FlushHandler, dict(aggregator=aggregator)),
    ])


def serve_forever():
    app = make_app()
    app.listen(port=FRIEDRICH_HTTP_PORT, address=FRIEDRICH_HTTP_ADDRESS)
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    serve_forever()
